=======
Sources
=======

.. line-block::

    Théorie : Blog « Création Monétaire »
    https://www.creationmonetaire.info

    Histoire, données : L'encyclopédie libre Wikipédia
    https://www.wikipedia.org

    Données Euro : Banque Centrale Européenne
    https://www.ecb.int

    Données monétaires des États-Unis
    https://www.shadowstats.com

    Yoland Bresson, économiste fondateur de l'AIRE
    http://www.revenudexistence.org

    Olivier Auber et la « perspective numérique »
    https://perspective-numerique.net

    Les images de la TRM sont toutes soit de Luc Fievet (libre), de Wikimedia (Creative Common), ou directement de l'auteur (Creative Common).
