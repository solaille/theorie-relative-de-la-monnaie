#!/usr/bin/env bash

# get new version
VERSION=$1
echo "Create new commit/tag for version: ${VERSION}"

if [[ $1 =~ ^[0-9]+.[0-9]+.[0-9]+[0-9a-z]*$ ]]; then
  # update version in package __init__.py
  sed -i "s/.*/${VERSION}/g" VERSION
  sed -i "s/VERSION: \".*\"/VERSION: \"${VERSION}\"/g" .gitlab-ci.yml
  # commit changes and add version tag
  git commit VERSION .gitlab-ci.yml -m "${VERSION}"
  git tag "${VERSION}" -a -m "${VERSION}"
else
  echo "Wrong version format"
fi
